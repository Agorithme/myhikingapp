# Hiking App

## Description
Hiking App est une application mobile développée en Flutter qui offre aux utilisateurs la possibilité de découvrir et de partager des itinéraires de randonnée. Elle intègre Google Maps pour une visualisation et une interaction cartographique, permettant aux utilisateurs d'ajouter et de visualiser des itinéraires de randonnée sur une carte interactive.

## Fonctionnalités
- **Explorer des randonnées** : Parcourez une liste de randonnées avec des informations essentielles comme le nom, la distance, et la difficulté.
- **Détails des randonnées** : Obtenez des informations détaillées sur une randonnée sélectionnée.
- **Ajouter des randonnées** : Utilisez la carte pour ajouter de nouveaux itinéraires en sélectionnant des points géographiques.
- **Visualisation sur carte** : Les itinéraires sont affichés sur Google Maps, offrant une interface intuitive pour la navigation.

## Prérequis
Pour exécuter cette application, vous aurez besoin de :
- Flutter (vérifiez l'installation avec `flutter doctor`)
- Une clé API valide pour Google Maps

## Installation
Suivez ces étapes pour installer et configurer l'application :

1. **Clonage du dépôt** :
   ```bash
   git clone https://gitlab.com/Agorithme/myhikingapp.git
   cd hiking_app
