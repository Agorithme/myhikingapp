import 'package:flutter/material.dart';
import 'hike_detail_page.dart';
import 'map_screen.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Map<String, dynamic>> hikes = [
      {"name": "Cirque de Gavarnie", "distance": "10 km", "difficulty": "Easy", "latitude": 42.7355, "longitude": -0.0000},
      {"name": "Pic du Midi d'Ossau", "distance": "12 km", "difficulty": "Hard", "latitude": 42.8400, "longitude": -0.4200},
      {"name": "Lac de Gaube", "distance": "7 km", "difficulty": "Medium", "latitude": 42.8696, "longitude": -0.1450},
      {"name": "Refuge et Lac d'Orédon", "distance": "5 km", "difficulty": "Easy", "latitude": 42.7794, "longitude": 0.1186},
      {"name": "La Brèche de Roland", "distance": "16 km", "difficulty": "Hard", "latitude": 42.6833, "longitude": -0.0333},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Available Hikes'),
      ),
      body: ListView.builder(
        itemCount: hikes.length,
        itemBuilder: (context, index) {
          final hike = hikes[index];
          return ListTile(
            title: Text(hike['name']),
            subtitle: Text('Distance: ${hike['distance']} - Difficulty: ${hike['difficulty']}'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HikeDetailPage(hike: hike),
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => MapScreen(),
            ),
          );
          if (result != null) {
            setState(() {
              hikes.add(result);
            });
          }
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
    );
  }
}
