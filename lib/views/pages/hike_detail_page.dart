import 'package:flutter/material.dart';
import 'map_screen.dart';

class HikeDetailPage extends StatelessWidget {
  final Map<String, dynamic> hike;

  const HikeDetailPage({Key? key, required this.hike}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(hike['name']),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(height: 20),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text("Details", style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Distance: ${hike['distance']}"),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Difficulty: ${hike['difficulty']}"),
          ),
          const Spacer(),
          Center(
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MapScreen(hike: hike),
                  ),
                );
              },
              child: const Text('View on Map'),
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
