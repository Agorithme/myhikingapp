import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget {
  final Map<String, dynamic>? hike;

  const MapScreen({super.key, this.hike});

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng? _selectedPosition;
  Set<Marker> _markers = {};
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _distanceController = TextEditingController();
  final TextEditingController _difficultyController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _distanceController.dispose();
    _difficultyController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    if (widget.hike != null) {
      _selectedPosition =
          LatLng(widget.hike!['latitude'], widget.hike!['longitude']);
      _markers.add(Marker(
        markerId: MarkerId('existingHike'),
        position: _selectedPosition!,
        infoWindow: InfoWindow(title: widget.hike!['name']),
      ));
    }
  }

  void _onMapTapped(LatLng position) {
    setState(() {
      _selectedPosition = position;
      _markers = {
        Marker(
          markerId: MarkerId('newHike'),
          position: position,
          draggable: true,
        )
      };
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.hike != null
            ? 'View Map - ${widget.hike!['name']}'
            : 'Add New Hike'),
      ),
      body: Stack(
        children: [
          GoogleMap(
            onMapCreated: (GoogleMapController controller) {},
            onTap: widget.hike == null ? _onMapTapped : null,
            markers: _markers,
            initialCameraPosition: CameraPosition(
              target: _selectedPosition ?? LatLng(42.7355, -0.0000),
              zoom: 15,
            ),
          ),
          if (_selectedPosition != null) _buildInputForm(),
        ],
      ),
      floatingActionButton: widget.hike == null
          ? FloatingActionButton(
              onPressed: () {
                if (_selectedPosition != null) {
                  Navigator.pop(context, {
                    'name': _nameController.text.isEmpty
                        ? 'New Hike'
                        : _nameController.text,
                    'distance': _distanceController.text,
                    'difficulty': _difficultyController.text,
                    'latitude': _selectedPosition!.latitude,
                    'longitude': _selectedPosition!.longitude,
                  });
                }
              },
              child: Icon(Icons.check),
              backgroundColor: Colors.green,
            )
          : null,
    );
  }

  Widget _buildInputForm() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        padding: EdgeInsets.all(20),
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _nameController,
              decoration: InputDecoration(
                labelText: 'Name',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 10),
            TextField(
              controller: _distanceController,
              decoration: InputDecoration(
                labelText: 'Distance',
                border: OutlineInputBorder(),
              ),
              keyboardType: TextInputType.number,
            ),
            SizedBox(height: 10),
            TextField(
              controller: _difficultyController,
              decoration: InputDecoration(
                labelText: 'Difficulty',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
